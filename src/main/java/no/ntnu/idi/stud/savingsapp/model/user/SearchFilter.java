package no.ntnu.idi.stud.savingsapp.model.user;

/**
 * Enum representing the search filter.
 */
public enum SearchFilter {

	NON_FRIENDS, FRIENDS

}
