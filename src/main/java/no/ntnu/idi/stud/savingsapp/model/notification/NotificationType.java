package no.ntnu.idi.stud.savingsapp.model.notification;

/**
 * Enum representing the type associated with a notification.
 */
public enum NotificationType {

	BADGE, FRIEND_REQUEST, COMPLETED_GOAL

}
