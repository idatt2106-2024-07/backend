package no.ntnu.idi.stud.savingsapp.dto.user;

import lombok.Data;

@Data
public class FeedbackRequestDTO {

	private String email;

	private String message;

}
