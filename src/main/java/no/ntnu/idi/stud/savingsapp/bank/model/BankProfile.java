package no.ntnu.idi.stud.savingsapp.bank.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Entity that represents a bank profile in the system. This profile consists of both an
 * id and a social security number.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "bank_profile")
public class BankProfile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bank_profile_id")
	private Long id;

	@Column(name = "ssn")
	private Long ssn;

	@OneToMany()
	@JsonManagedReference
	@JoinColumn(name = "bank_profile_id")
	private List<Account> accounts;

}
