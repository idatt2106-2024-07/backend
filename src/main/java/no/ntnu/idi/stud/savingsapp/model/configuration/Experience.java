package no.ntnu.idi.stud.savingsapp.model.configuration;

/**
 * Enum representing the level of experience.
 */
public enum Experience {

	NONE, SOME, EXPERT

}
