package no.ntnu.idi.stud.savingsapp.bank.dto;

import lombok.Data;

/**
 * Represents a request containing a bank account.
 */
@Data
public class AccountRequestDTO {

	private Long ssn;

}
