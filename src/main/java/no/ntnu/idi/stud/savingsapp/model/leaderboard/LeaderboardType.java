package no.ntnu.idi.stud.savingsapp.model.leaderboard;

/**
 * Enum representing the type of Leaderboard.
 */
public enum LeaderboardType {

	TOTAL_POINTS, CURRENT_STREAK, TOP_STREAK

}
