package no.ntnu.idi.stud.savingsapp.model.leaderboard;

/**
 * Enum representing the Leaderboard filter.
 */
public enum LeaderboardFilter {

	GLOBAL, FRIENDS

}
