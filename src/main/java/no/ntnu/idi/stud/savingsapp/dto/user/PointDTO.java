package no.ntnu.idi.stud.savingsapp.dto.user;

import lombok.Data;

@Data
public class PointDTO {

	private int currentPoints;

	private int totalEarnedPoints;

}
