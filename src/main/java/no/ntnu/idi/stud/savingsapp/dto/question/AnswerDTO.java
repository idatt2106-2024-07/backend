package no.ntnu.idi.stud.savingsapp.dto.question;

import lombok.Data;

@Data
public class AnswerDTO {

	String answer_text;

}
