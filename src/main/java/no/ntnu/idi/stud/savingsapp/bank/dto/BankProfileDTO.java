package no.ntnu.idi.stud.savingsapp.bank.dto;

import lombok.Data;

/**
 * Represents a bank profile.
 */
@Data
public class BankProfileDTO {

	private Long ssn;

}
