package no.ntnu.idi.stud.savingsapp.model.user;

/**
 * Enum representing the role associated witha user.
 */
public enum Role {

	USER,

	ADMIN

}
