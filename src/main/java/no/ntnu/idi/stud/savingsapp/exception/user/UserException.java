package no.ntnu.idi.stud.savingsapp.exception.user;

/**
 * Exception thrown regarding a user in the system.
 */
public final class UserException extends RuntimeException {

	/**
	 * Constructs a UserException with the default message.
	 */
	public UserException(String string) {
		super(string);
	}

}
