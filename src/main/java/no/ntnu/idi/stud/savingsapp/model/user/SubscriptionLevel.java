package no.ntnu.idi.stud.savingsapp.model.user;

/**
 * Enum representing the type of subscription level.
 */
public enum SubscriptionLevel {

	DEFAULT, PREMIUM, NO_ADS

}
